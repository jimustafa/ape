## Lithium

As a first example, we will take a Lithium atom. Using a text editor, create the
following input `inp.ape`:
```no-lang
Title = "Lithium"
CalculationMode = ae
Verbose = 30

NuclearCharge = 3

%Orbitals
  "He"
  2  |  0  |  1
  2  |  1  |  0
%
```
Then run APE. The output should look like this:

```no-lang
                     APE - Atomic Pseudopotentials Engine

                  Program started on 2012/06/20 at 12:01:41

    Compilation Info
      Version: 2.x
      Revision:
      Build time: Wed Jun 20 12:00:49 WEST 2012
      C compiler: gcc
      C compiler flags: -g -O2 -I/usr/include
      Fortran compiler: gfortran
      Fortran compiler flags: -Wall -fbounds-check

    Calculation Type:
      Atomic Calculation

    Setting units
      Input units system:  Atomic Units
      Output units system: Atomic Units

    Eigensolver Info
      Method:    Brent's method
      Tolerance:  1.000E-08


                        -- All Electron Calculation --

    Initializing ODE Integrator
      ODE Stepping function: Embedded 8th order Runge-Kutta Prince-Dormand
                             method with 9th order error estimate
      ODE Integrator tolerance:  1.000E-12
      ODE Integrator maximum number of steps: 500000

    General Information about the atom:
      Symbol: Li
      Theory Level: DFT
      Wave-equation: Schrodinger
      Spin mode: unpolarized
      Nuclear charge:   3.00
      Total charge:     0.00
      Configuration    : State   Occupation
                           1s       2.00
                           2s       1.00
                           2p       0.00

    Exchange-Correlation model:
      Correlation: Perdew & Wang (LDA)
      Exchange:    Slater exchange (LDA)

    Mesh information:
      Type: logarithmic [ri = b*exp(a*i)]
      Mesh starting point:   1.73E-05 b
      Mesh outmost point:     51.962 b
      Mesh parameters (a, b):  7.49453E-02,  1.60699E-05
      Mesh number of points:    200
      Derivatives Method: Cubic slines

    Starting SCF process
      Convergence tolerance: ConvAbsDens ConvRelDens
                              0.000E+00   1.000E-08
                             ConvAbsEnergy ConvRelEnergy
                              0.000E+00   0.000E+00
      Smearing: fixed occupancies
      Maximum number of iterations:    300
      Mixing scheme: Modified Broyden's Method
      Mixing: 0.300

    Performing SCF Cycle

      Final results for SCF procedure:

                  Program finished on 2012/06/20 at 12:01:42
```

Now take a look at the working directory. It should include the
following files and directories:

```no-lang
 drwxrwxr-x    2 user   group       4096 2012-06-20 12:01 ae
 -rw-rw-r--    1 user   group        198 2012-06-20 12:01 inp.ape
 -rw-rw-r--    1 user   group       1022 2012-06-20 12:01 parser.log
```

Besides the `inp.ape` and `parser.log` files there is now a new directory named
`ae`. In that directory you will find the following files:

```no-lang
 -rw-r--r-- 1 user group 24885 2012-06-20 12:01 data
 -rw-r--r-- 1 user group 14665 2012-06-20 12:01 density
 -rw-r--r-- 1 user group  1549 2012-06-20 12:01 info
 -rw-r--r-- 1 user group  7396 2012-06-20 12:01 tau
 -rw-r--r-- 1 user group  7567 2012-06-20 12:01 v_c
 -rw-r--r-- 1 user group  7361 2012-06-20 12:01 v_ext
 -rw-r--r-- 1 user group  7361 2012-06-20 12:01 v_hxc
 -rw-r--r-- 1 user group  7567 2012-06-20 12:01 v_x
 -rw-r--r-- 1 user group 10938 2012-06-20 12:01 wf-1s
 -rw-r--r-- 1 user group 10938 2012-06-20 12:01 wf-2p
 -rw-r--r-- 1 user group 10966 2012-06-20 12:01 wf-2s
```

The file `data` is a binary file containing the all-electron calculation
data. This file is necessary if you want to generate pseudopotentials without
having to solve again the all-electron equations. The file `info` contains some
information about the all-electron calculation and it should be
self-explanatory. The files `density` and `tau` contain the eletronic density
and the kinetic energy density in a format suitable for plotting. The files
whose name start with `v_` contain the different components of the Kohn-Sham
potential. Finally, the `wf` files contain the radial wavefuntions.

Next we will generate some pseudopotentials using the Hamann scheme.  Create the
following `inp.ape` file:

```no-lang
Title = "Lithium"
CalculationMode = pp
Verbose = 30

%PPComponents
  2  |  0  | 0.00 | ham
  2  |  1  | 0.00 | ham
%
```

This time the output should look like this:

```no-lang
                     APE - Atomic Pseudopotentials Engine

                  Program started on 2012/06/20 at 12:06:13

    Compilation Info
      Version: 2.x
      Revision:
      Build time: Wed Jun 20 12:00:49 WEST 2012
      C compiler: gcc
      C compiler flags: -g -O2 -I/usr/include
      Fortran compiler: gfortran
      Fortran compiler flags: -Wall -fbounds-check

    Calculation Type:
      PseudoPotential Generation

    Setting units
      Input units system:  Atomic Units
      Output units system: Atomic Units

    Eigensolver Info
      Method:    Brent's method
      Tolerance:  1.000E-08

    Initializing ODE Integrator
      ODE Stepping function: Embedded 8th order Runge-Kutta Prince-Dormand
                             method with 9th order error estimate
      ODE Integrator tolerance:  1.000E-12
      ODE Integrator maximum number of steps: 500000


                       -- Pseudopotential Generation --

    Pseudo atom information:
      Wavefunction info:
        State   Occupation    Node radius   Peak radius   Default core radius
         2s        1.00          0.842         3.012             1.807
         2p        0.00          0.000         3.771             1.509

    Pseudopotential Generation:
      State: 2s
        Scheme: Hamann
        Core radius:   1.807     Matching Radius:   5.486
        cl  =     0.1270413205
      State: 2p
        Scheme: Hamann
        Core radius:   1.509     Matching Radius:   4.381
        cl  =    -0.9176124519

    Pseudopotentials Self-Consistency:
      State  Eigenvalue [H ]    Norm Test   Slope Test
        2s        -0.10560      1.0000049   0.9999925
        2p        -0.04140      0.9998835   0.9998173

    Kleinman & Bylander Atom
      Local potential is a Vanderbilt function
           z     rcmax      v0          v1          v2          v3
          1.00    5.09   -0.491383   -0.057906    0.001118   -0.000010
      Non-local components:
        State   KB Energy [H ]   KB Cosine
          2s        1.0840         0.0761
          2p       -0.4609        -0.2732

      Ghost state analysis:
        State: 2s
          KB energy > 0; E0 < Eref < E1  =>  No ghost states
          Local potential eigenvalues:   -0.1215 (E0)    -0.0050 (E1)
          Reference energy:              -0.1056 (Eref)
        State: 2p
          KB energy < 0; Eref < E0       =>  No ghost states
          Local potential eigenvalues:   -0.0248 (E0)     0.0000 (E1)
          Reference energy:              -0.0414 (Eref)

      Localization radii [b]:
        Local:  3.77
        l = 0:  4.06
        l = 1:  3.77

                  Program finished on 2012/06/20 at 12:06:14
```

At the end of the run there should be a new file named `Li.UPF` and two new
directories named `pp` and `kb` in the working directory. The file `Li.UPF`
contains the pseudopotential information in the Quantum-Espresso unified
pseudopotential format (UPF). The `pp` directory should include the following files:

```no-lang
 -rw-r--r-- 1 user group 26463 2012-06-20 12:06 data
 -rw-r--r-- 1 user group 14665 2012-06-20 12:06 density
 -rw-r--r-- 1 user group   696 2012-06-20 12:06 info
 -rw-r--r-- 1 user group  7361 2012-06-20 12:06 pp-p
 -rw-r--r-- 1 user group  7361 2012-06-20 12:06 pp-s
 -rw-r--r-- 1 user group  7396 2012-06-20 12:06 tau
 -rw-r--r-- 1 user group  7567 2012-06-20 12:06 v_c
 -rw-r--r-- 1 user group  7361 2012-06-20 12:06 v_hxc
 -rw-r--r-- 1 user group  7567 2012-06-20 12:06 v_x
 -rw-r--r-- 1 user group 10938 2012-06-20 12:06 wf-2p
 -rw-r--r-- 1 user group 10938 2012-06-20 12:06 wf-2s
```

The file `data` is a binary file containing the pseudopotential calculation
data. This file is necessary if you want to perform further tests on the
pseudopotential transferability. The file `info` contains some information about
the pseudopotential generation and it should be self-explanatory. The files
`density` and `tau` contain the valence eletronic density and kinetic energy
density in a format suitable for plotting. The `wf` files contain the radial
pseudo-wavefuntions and the `pp` files contain the ionic
pseudopotentials. Finally, the files whose name start with `v_` contain the
different components of the Kohn-Sham potential evaluated for the pseudo valence
states.

As for the `kb` directory should include the following files:

```no-lang
 -rw-r--r-- 1 user group  819 2012-06-20 12:06 info
 -rw-r--r-- 1 user group 7361 2012-06-20 12:06 kb-local
 -rw-r--r-- 1 user group 7364 2012-06-20 12:06 kb-p
 -rw-r--r-- 1 user group 7364 2012-06-20 12:06 kb-s
```

The `info` contains some information about the Kleinman-Bylander projectors and
it should be self-explanatory. The file `kb-local` contains the local component
used to generated the Kleinman-Bylander projectors, while the other `kb` files
contain the projectors.

After generating the pseudopotentials, one should test them. One simple test is
to compare the logarithmic derivative of the wavefunctions as a function of the
orbital energy at a given diagnostic radius. The following input file will to
precisely that:

```no-lang
Title = "Lithium"
CalculationMode = pp_test
Verbose = 30

PPTests = ld
```

In this case the output should look like this:

```no-lang
                     APE - Atomic Pseudopotentials Engine

                  Program started on 2012/06/20 at 13:55:25

    Compilation Info
      Version: 2.x
      Revision:
      Build time: Wed Jun 20 12:00:49 WEST 2012
      C compiler: gcc
      C compiler flags: -g -O2 -I/usr/include
      Fortran compiler: gfortran
      Fortran compiler flags: -Wall -fbounds-check

    Calculation Type:
      PseudoPotential Test

    Setting units
      Input units system:  Atomic Units
      Output units system: Atomic Units

    Eigensolver Info
      Method:    Brent's method
      Tolerance:  1.000E-08

    Initializing ODE Integrator
      ODE Stepping function: Embedded 8th order Runge-Kutta Prince-Dormand
                             method with 9th order error estimate
      ODE Integrator tolerance:  1.000E-12
      ODE Integrator maximum number of steps: 500000


                        -- Pseudopotential Testing --

    Initializing ODE Integrator
      ODE Stepping function: Embedded 8th order Runge-Kutta Prince-Dormand
                             method with 9th order error estimate
      ODE Integrator tolerance:  1.000E-12
      ODE Integrator maximum number of steps: 500000

    Logarithmic Derivatives:
      Diagnostic radius:  2.320 b
      Energy step:    Adaptive
      Computing logarithmic derivative for states: s
        Minimum energy: -1.106 H
        Maximum energy:  0.894 H
      Computing logarithmic derivative for states: p
        Minimum energy: -1.041 H
        Maximum energy:  0.959 H

                  Program finished on 2012/06/20 at 13:55:25
```

Notice that the choice of energy range and diagnostic radius was done
automatically (check the description of the corresponding variable for more
information about how this is done). At the end of the run there should be a new
directory named `tests`. This directory should contain the following files:
```no-lang
 -rw-r--r-- 1 user group  297 2012-06-20 13:55 info
 -rw-r--r-- 1 user group 3916 2012-06-20 13:55 ld-p
 -rw-r--r-- 1 user group 3700 2012-06-20 13:55 ld-s
```

The file `info` contains some information about the tests and it should be
self-explanatory. The files `ld-` contain the all-electron and pseudo
wavefunction logarithmic derivatives as a function of the orbital energy.
