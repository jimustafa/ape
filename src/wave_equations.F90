!! Copyright (C) 2004-2014 M. Oliveira, F. Nogueira
!! Copyright (C) 2011-2012 T. Cerqueira
!! Copyright (C) 2014 P. Borlido
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

module wave_equations_m
  use global_m
  use oct_parser_m
  use messages_m
  use splines_m
  use mesh_m
  use utilities_m
  use quantum_numbers_m
  use potentials_m
  use ode_integrator_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
    module procedure integrator_copy
  end interface


                    !---Derived Data Types---!

  type integrator_t
    integer  :: nstepmax
    integer  :: stepping_function
    real(R8) :: tol
  end type integrator_t


                   !---Global Variables---!

  integer, parameter :: SCHRODINGER  = 1, &
                        SCALAR_REL   = 2, &
                        DIRAC        = 3


                    !---Public/Private Statements---!

  private
  public :: integrator_t, &
            integrator_null, &
            integrator_init, &
            integrator_end, &
            assignment(=), &
            hydrogen_eigenvalue, &
            wave_equation_ld_diff, &
            wavefunctions, &
            wavefunctions_norm, &
            wavefunctions_n_bound_states, &
            hamann_ld, &
            hamann_wavefunction, &
            wave_equation_emin, &
            wave_equation_emax, &
            wave_function_cutoff, &
            SCHRODINGER, &
            DIRAC, &
            SCALAR_REL


contains

  !-----------------------------------------------------------------------
  !>  Nullifies and sets to zero all the components of the integrator.    
  !-----------------------------------------------------------------------
  subroutine integrator_null(integrator)
    type(integrator_t), intent(out) :: integrator

    call push_sub("integrator_null")

    integrator%nstepmax = 0
    integrator%stepping_function = 0
    integrator%tol = M_ZERO

    call pop_sub()
  end subroutine integrator_null

  !-----------------------------------------------------------------------
  !> Initializes integrator objects by reading some parameters from the   
  !> input file and by initializing GSL ODE solver objects.               
  !> Two objects are initialized: one for single precision integrations   
  !> and a second one for double precision integrations.                  
  !-----------------------------------------------------------------------
  subroutine integrator_init(integrator_sp, integrator_dp)
    type(integrator_t), intent(out) :: integrator_sp !< single-precision integrator object
    type(integrator_t), intent(out) :: integrator_dp !< double-precision integrator object

    real(R8) :: tol

    call push_sub("integrator_init")

    message(1) = ""
    message(2) = "Initializing ODE Integrator"
    call write_info(2)

    !Which stepping function will we use
    integrator_sp%stepping_function = oct_parse_f90_int('ODESteppingFunction', ODE_RKPD8)
    select case (integrator_sp%stepping_function)
    case (ODE_RK2)
      message(1) = "  ODE Stepping function: Embedded 2nd order Runge-Kutta"
      message(2) = "                         with 3rd order error estimate"
      call write_info(2,20)
    case (ODE_RK4)
      message(1) = "  ODE Stepping function: 4th order (classical) Runge-Kutta method"
      call write_info(1,20)
    case (ODE_RKF4)
      message(1) = "  ODE Stepping function: Embedded 4th order Runge-Kutta-Fehlberg "
      message(2) = "                         method with 5th order error estimate"
      call write_info(2,20)
    case (ODE_RKCK4)
      message(1) = "  ODE Stepping function: Embedded 4th order Runge-Kutta Cash-Karp"
      message(2) = "                         method with 5th order error estimate"
      call write_info(2,20)
    case (ODE_RKPD8)
      message(1) = "  ODE Stepping function: Embedded 8th order Runge-Kutta Prince-Dormand"
      message(2) = "                         method with 9th order error estimate"
      call write_info(2,20)
!    case (6)
!      message(1) = "  Stepping function: Implicit 4th order Runge-Kutta at Gaussian points"
!      call write_info(1,20)
!    case (7)
!      message(1) = "  Stepping function: M=2 implicit Gear method"
!      call write_info(1,20)
    case default
       message(1) = "ODESteppingFunction must be between 1 and 5."
       call write_fatal(1)
    end select

    !Read the ODE integrator tolerance
    tol = oct_parse_f90_double('ODEIntTolerance', 1.0E-12_r8)
    if (tol <= M_ZERO) then
      message(1) = "ODEIntTolerance must be greater than zero."
      call write_fatal(1)
    end if
    write(message(1),'(2X,"ODE Integrator tolerance: ",ES10.3E2)') tol
    call write_info(1,20)

    !Read the maximum number of steps the ODE integrator is allowed to take
    integrator_sp%nstepmax = oct_parse_f90_int('ODEMaxSteps', 500000)
    if (integrator_sp%nstepmax <= M_ZERO) then
      message(1) = "ODEMaxSteps must be greater than zero."
      call write_fatal(1)
    end if
    write(message(1),'(2X,"ODE Integrator maximum number of steps: ",I6)') integrator_sp%nstepmax
    call write_info(1,20)

    integrator_sp%tol = sqrt(tol)

    integrator_dp%nstepmax = integrator_sp%nstepmax
    integrator_dp%stepping_function = integrator_sp%stepping_function
    integrator_dp%tol = tol

    call pop_sub()
  end subroutine integrator_init

  !-----------------------------------------------------------------------
  !> Copies integrator_b to integrator_a.                                 
  !-----------------------------------------------------------------------
  subroutine integrator_copy(integrator_a, integrator_b)
    type(integrator_t), intent(inout) :: integrator_a
    type(integrator_t), intent(in)    :: integrator_b

    call push_sub("integrator_copy")

    call integrator_end(integrator_a)

    !Copy data
    integrator_a%nstepmax = integrator_b%nstepmax
    integrator_a%stepping_function = integrator_b%stepping_function
    integrator_a%tol = integrator_b%tol

    call pop_sub()
  end subroutine integrator_copy

  !-----------------------------------------------------------------------
  !>
  !-----------------------------------------------------------------------
  subroutine integrator_end(integrator)
    type(integrator_t), intent(inout) :: integrator

    call push_sub("integrator_end")

    integrator%nstepmax = 0
    integrator%stepping_function = 0
    integrator%tol = M_ZERO

    call pop_sub()
  end subroutine integrator_end

  !-----------------------------------------------------------------------
  !> Returns the eigenvalues of the hidrogen-like atoms.                  
  !-----------------------------------------------------------------------
  function hydrogen_eigenvalue(qn, wave_eq, z)
    type(qn_t), intent(in) :: qn      !< set of quantum numbers
    integer,    intent(in) :: wave_eq !< wave-equation
    real(R8),   intent(in) :: z       !< nuclear charge
    real(R8) :: hydrogen_eigenvalue

    real(R8) :: e_nr

    select case (wave_eq)
    case (SCHRODINGER)
      hydrogen_eigenvalue = -z**2/(M_TWO*real(qn%n,R8)**2)
    case (DIRAC)
      hydrogen_eigenvalue = M_C2*(M_ONE + z**2/M_C2/(real(qn%n,R8) - M_HALF &
           - qn%j + sqrt((qn%j + M_HALF)**2 - z**2/M_C2))**2)**(-M_HALF) - M_C2
    case (SCALAR_REL)
      if (qn%l == 0) then
        hydrogen_eigenvalue = M_C2*(M_ONE + z**2/M_C2/(real(qn%n,R8) - M_ONE &
                            + sqrt(M_ONE - z**2/M_C2))**2)**(-M_HALF) - M_C2
      else
        e_nr = -z**2/(M_TWO*real(qn%n,R8)**2)
        hydrogen_eigenvalue = e_nr - e_nr**2/M_TWO/M_C2*(M_FOUR*real(qn%n,R8)/(real(qn%l,R8) + M_HALF) - M_THREE)
      end if
    end select

  end function hydrogen_eigenvalue

  !-----------------------------------------------------------------------
  !> Returns the difference of the logaritmic derivative computed by      
  !> integrating the wave-equation inward and outward.                    
  !-----------------------------------------------------------------------
  function wave_equation_ld_diff(qn, e, wave_eq, potential, integrator, nnodes)
    type(qn_t),         intent(in)    :: qn         !< set of quantum numbers
    real(R8),           intent(in)    :: e          !< energy
    integer,            intent(in)    :: wave_eq    !< wave-equation to integrate
    type(potential_t),  intent(in)    :: potential  !< potential to use in the wave-equation
    type(integrator_t), intent(inout) :: integrator !< integrator object
    integer,            intent(out)   :: nnodes     !< number of nodes of the wave-function
    real(R8) :: wave_equation_ld_diff

    integer  :: i, nstep_out, nstep_in, ode, nnodes1, nnodes2
    real(R8) :: r0, ri, rinf
    type(ode_integrator_t) :: odeint
    real(R8), pointer :: r_out(:), f_out(:,:)
    real(R8), pointer :: r_in(:),  f_in(:,:)

    call push_sub("wave_equation_ld_diff")

    !Set the parameters needed to compute the derivatives of the functions
    ode = wave_equation_to_ode(wave_eq, qn, potential)
    call ode_integrator_null(odeint)
    call ode_integrator_init(odeint, ode, integrator%stepping_function, &
                             integrator%nstepmax, integrator%tol, qn, e, potential)

    !Set initial, final, and intermidiate points
    r0 = potential_rmin(potential)
    ri = classical_turning_point(potential, e, qn)
    rinf = ode_practical_infinity(odeint, ri, integrator%tol)

    !Outward and inward integrations
    call ode_integration(odeint, r0,   ri, nstep_out, r_out, f_out)
    call ode_integration(odeint, rinf, ri, nstep_in,  r_in,  f_in)

    !Match inward and outward functions at the classical turning point
    call ode_match_functions(odeint, nstep_out, nstep_in, f_out, f_in)

    !Calculate functions mismatch
    wave_equation_ld_diff = &
         ode_function_mismatch(odeint, ri, f_out(nstep_out,:), f_in(nstep_in,:))

    !Unset the derivatives parameters
    call ode_integrator_end(odeint)

    !Calculate the number of nodes
    if (ode == ODE_DIRAC_POL2) then
      nnodes1 = 0
      nnodes2 = 0
      do i = 1, nstep_out - 1
        if ( (f_out(i,1)+f_out(i,5))*(f_out(i + 1,1)+f_out(i+1,5)) < M_ZERO) nnodes1 = nnodes1 + 1
        if ( (f_out(i,3)+f_out(i,7))*(f_out(i + 1,3)+f_out(i+1,7)) < M_ZERO) nnodes2 = nnodes2 + 1
      end do
      nnodes = min(nnodes1, nnodes2)
    else
      nnodes = 0
      do i = 1, nstep_out - 1
        if (f_out(i,1)*f_out(i + 1,1) < M_ZERO) nnodes = nnodes + 1
      end do
    end if

    !Deallocate pointers
    deallocate(r_out, r_in, f_out, f_in)

    call pop_sub()
  end function wave_equation_ld_diff

  !-----------------------------------------------------------------------
  !> For a given energy, computes the wave-functions. The energy does not 
  !> need to be an eigenvalue.                                            
  !-----------------------------------------------------------------------
  subroutine wavefunctions(qn, e, wave_eq, m, potential, integrator, wf, wfp)
    type(qn_t),         intent(in)    :: qn         !< set of quantum numbers
    real(R8),           intent(in)    :: e          !< energy
    integer,            intent(in)    :: wave_eq    !< wave-equation to use
    type(mesh_t),       intent(in)    :: m          !< mesh
    type(potential_t),  intent(in)    :: potential  !< potential to use in the wave-equation
    type(integrator_t), intent(inout) :: integrator !< integrator object
    real(R8),           intent(inout) :: wf(:, :)   !< wavefunction
    real(R8),           intent(inout) :: wfp(:, :)  !< wavefunction derivative

    integer  :: ode, wf_dim, i, nstep_out, nstep_in, nstep_tot, r_tot_max
    real(R8) :: r0, ri, rinf, factor
    type(ode_integrator_t) :: odeint
    real(R8), pointer     :: r_out(:), f_out(:,:)
    real(R8), pointer     :: r_in(:),  f_in(:,:)
    real(R8), allocatable :: r_tot(:), wf_tot(:,:), wfp_tot(:,:)

    call push_sub("wavefunctions")

    !Set the parameters needed to compute the derivatives of the functions
    ode = wave_equation_to_ode(wave_eq, qn, potential)
    call ode_integrator_null(odeint)
    call ode_integrator_init(odeint, ode, integrator%stepping_function, &
                             integrator%nstepmax, integrator%tol, qn, e, potential)

    !Set initial, final, and intermidiate points
    r0 = potential_rmin(potential)
    ri = classical_turning_point(potential, e, qn)
    rinf = ode_practical_infinity(odeint, ri, integrator%tol)

    !Outward and inward integrations
    call ode_integration(odeint, r0,   ri, nstep_out, r_out, f_out)
    call ode_integration(odeint, rinf, ri, nstep_in,  r_in,  f_in)

    !Match inward and outward functions at the classical turning point
    call ode_match_functions(odeint, nstep_out, nstep_in, f_out, f_in)
    
    !Get the inward and outward wavefunctions on a single array
    wf_dim = qn_wf_dim(qn)
    nstep_tot = nstep_in + nstep_out - 1
    allocate(r_tot(nstep_tot), wf_tot(nstep_tot, wf_dim), wfp_tot(nstep_tot, wf_dim))
    do i = 1, nstep_out
      r_tot(i) = r_out(i)
      call ode_function_to_wavefunction(odeint, &
                             r_tot(i), f_out(i,:), wf_tot(i,:), wfp_tot(i,:))
    end do
    do i = 1,nstep_in-1
      r_tot(nstep_out+i) = r_in(nstep_in-i)
      call ode_function_to_wavefunction(odeint, &
                             r_tot(nstep_out+i), f_in(nstep_in-i,:), &
                             wf_tot(nstep_out+i,:), wfp_tot(nstep_out+i,:))
    end do

    !Unset the derivatives parameters
    call ode_integrator_end(odeint)

    !Get the wavefunctions in the correct mesh
    r_tot_max = locate(m%r, r_tot(nstep_tot), 0)
    do i = 1, wf_dim
      call spline_mesh_transfer(nstep_tot, r_tot, wf_tot(:,i), r_tot_max, m%r(1:r_tot_max), wf(1:r_tot_max,i), 3)
      call spline_mesh_transfer(nstep_tot, r_tot, wfp_tot(:,i), r_tot_max, m%r(1:r_tot_max), wfp(1:r_tot_max,i), 3)
    end do
    wf(r_tot_max + 1:m%np,:) = M_ZERO
    wfp(r_tot_max + 1:m%np,:) = M_ZERO

    !Deallocate pointers
    deallocate(r_tot, r_out, r_in, wf_tot, wfp_tot, f_out, f_in)

    !Normalize the wavefunction
    factor = wavefunctions_norm(wave_eq, m, wf)
    wf = wf/sqrt(factor)
    wfp = wfp/sqrt(factor)
    where (abs(wf)  < wave_function_cutoff(integrator)) wf = M_ZERO
    factor = wavefunctions_norm(wave_eq, m, wf)
    wf = wf/sqrt(factor)
    wfp = wfp/sqrt(factor)
    where (abs(wfp) < wave_function_cutoff(integrator)) wfp = M_ZERO

    call pop_sub()
  end subroutine wavefunctions

  !-----------------------------------------------------------------------
  !> Returns the norm of the wave-function.                               
  !-----------------------------------------------------------------------
  function wavefunctions_norm(wave_eq, m, wf)
    integer,      intent(in) :: wave_eq
    type(mesh_t), intent(in) :: m
    real(R8),     intent(in) :: wf(:,:) ! wf(m%np, wf_dim)
    real(R8) :: wavefunctions_norm
    
    select case (wave_eq)
    case (SCHRODINGER, SCALAR_REL)
      wavefunctions_norm = mesh_integrate(m, wf(:,1)**2)
    case (DIRAC)
      wavefunctions_norm = mesh_integrate(m, sum(wf**2,dim=2))
    end select

  end function wavefunctions_norm

  !-----------------------------------------------------------------------
  !> Integrates the wave-equation outward up to a certain radius and then 
  !> computes the logarithmic derivative.                                 
  !-----------------------------------------------------------------------
  function wavefunctions_n_bound_states(qn, wave_eq, m, potential, integrator)
    type(qn_t),         intent(in)    :: qn         !< set of quantum numbers
    integer,            intent(in)    :: wave_eq    !< wave-equation to use
    type(mesh_t),       intent(in)    :: m          !< mesh
    type(potential_t),  intent(in)    :: potential  !< potential to use in the wave-equation
    type(integrator_t), intent(inout) :: integrator !< integrator object
    integer :: wavefunctions_n_bound_states

    integer :: ode, i, nstep, nnodes
    real(R8) :: e
    type(ode_integrator_t) :: odeint
    real(R8), pointer :: r(:), f(:,:)

    call push_sub("wavefunctions_n_bound_states")

    e = wave_equation_emax(qn, potential)

    !Set the parameters needed to compute the derivatives of the functions
    ode = wave_equation_to_ode(wave_eq, qn, potential)
    call ode_integrator_null(odeint)
    call ode_integrator_init(odeint, ode, integrator%stepping_function, &
                             integrator%nstepmax, integrator%tol, qn, e, potential)

    !Outward integration
    call ode_integration(odeint, m%r(1), m%r(m%np), nstep, r, f)

    !Unset the derivatives parameters
    call ode_integrator_end(odeint)

    !Calculate the number of nodes
    nnodes = 0
    do i = 1, nstep-1
      if (f(i,1)*f(i+1,1) < M_ZERO) nnodes = nnodes + 1
    end do

    !Deallocate pointers
    deallocate(r, f)

    wavefunctions_n_bound_states = nnodes

    call pop_sub()
  end function wavefunctions_n_bound_states

  !-----------------------------------------------------------------------
  !> Integrates the wave-equation outward up to a certain radius and then 
  !> computes the logarithmic derivative.                                 
  !-----------------------------------------------------------------------
  function hamann_ld(qn, e, wave_eq, m, potential, integrator, nnodes)
    type(qn_t),         intent(in)    :: qn         !< set of quantum numbers
    real(R8),           intent(in)    :: e          !< energy
    integer,            intent(in)    :: wave_eq    !< wave-equation to use
    type(mesh_t),       intent(in)    :: m          !< mesh
    type(potential_t),  intent(in)    :: potential  !< potential to use in the wave-equation
    type(integrator_t), intent(inout) :: integrator !< integrator object
    integer,            intent(out)   :: nnodes     !< number of nodes of the wave-function
    real(R8) :: hamann_ld

    integer :: ode, i, nstep, wf_dim
    real(R8) :: rc
    type(ode_integrator_t) :: odeint
    real(R8), allocatable :: wf_rc(:), wfp_rc(:)
    real(R8), pointer :: r(:), f(:,:)

    call push_sub("hamann_ld")

    !Set the parameters needed to compute the derivatives of the functions
    ode = wave_equation_to_ode(wave_eq, qn, potential)
    call ode_integrator_null(odeint)
    call ode_integrator_init(odeint, ode, integrator%stepping_function, &
                             integrator%nstepmax, integrator%tol, qn, e, potential)

    rc = m%r(m%np)
    call ode_integration(odeint, potential_rmin(potential), rc, nstep, r, f)

    !Calculate the logaritmic derivative at rc
    wf_dim = qn_wf_dim(qn)
    allocate(wf_rc(wf_dim), wfp_rc(wf_dim))
    call ode_function_to_wavefunction(odeint, rc, f(nstep,:), wf_rc, wfp_rc)
    hamann_ld = wfp_rc(1)/wf_rc(1)
    deallocate(wf_rc, wfp_rc)

    !Unset the derivatives parameters
    call ode_integrator_end(odeint)

    !Calculate the number of nodes
    nnodes = 0
    do i = 1, nstep-1
      if (f(i,1)*f(i+1,1) < M_ZERO) nnodes = nnodes + 1
    end do

    !Deallocate pointers
    deallocate(r, f)

    call pop_sub()
  end function hamann_ld

  !-----------------------------------------------------------------------
  !> For a given energy, computes the wave-functions "a la Hamann":       
  !> integrate the wave-equation outward up to a certain radius and the   
  !> normalize it to one.                                                 
  !-----------------------------------------------------------------------
  subroutine hamann_wavefunction(qn, e, wave_eq, m, potential, integrator, wf, wfp)
    type(qn_t),         intent(in)    :: qn         !< set of quantum numbers
    real(R8),           intent(in)    :: e          !< energy
    integer,            intent(in)    :: wave_eq    !< wave-equation to use
    type(mesh_t),       intent(in)    :: m          !< mesh
    type(potential_t),  intent(in)    :: potential  !< potential to use in the wave-equation
    type(integrator_t), intent(inout) :: integrator !< integrator object
    real(R8),           intent(out)   :: wf(:, :)   !< wavefunction
    real(R8),           intent(out)   :: wfp(:, :)  !< wavefunction derivative

    integer :: ode, i, nstep_out, wf_dim
    real(R8) :: factor
    type(ode_integrator_t) :: odeint
    real(R8), allocatable :: wf_out(:,:), wfp_out(:,:)
    real(R8), pointer :: r_out(:), f_out(:,:)

    call push_sub("hamann_wavefunction")

    !Set the parameters needed to compute the derivatives of the functions
    ode = wave_equation_to_ode(wave_eq, qn, potential)
    call ode_integrator_null(odeint)
    call ode_integrator_init(odeint, ode, integrator%stepping_function, &
                             integrator%nstepmax, integrator%tol, qn, e, potential)

    !Outward integration
    call ode_integration(odeint, potential_rmin(potential), m%r(m%np), nstep_out, r_out, f_out)

    wf_dim = qn_wf_dim(qn)
    allocate(wf_out(nstep_out, wf_dim), wfp_out(nstep_out, wf_dim))
    do i = 1, nstep_out
      call ode_function_to_wavefunction(odeint, r_out(i), &
                                  f_out(i,:), wf_out(i,:), wfp_out(i,:))      
    end do

    !Unset the derivatives parameters
    call ode_integrator_end(odeint)

    !
    do i = 1, wf_dim
      call spline_mesh_transfer(nstep_out, r_out, wf_out(:,i),  m%np, m%r, wf(:,i),  3)
      call spline_mesh_transfer(nstep_out, r_out, wfp_out(:,i), m%np, m%r, wfp(:,i), 3)
    end do

    !Normalize the wavefunction
    factor = wavefunctions_norm(SCHRODINGER, m, wf)
    wf = wf/sqrt(factor)
    wfp = wfp/sqrt(factor)

    !Deallocate pointers
    deallocate(r_out, f_out, wf_out, wfp_out)

    call pop_sub()
  end subroutine hamann_wavefunction

  !-----------------------------------------------------------------------
  !> Returns the lower bound of the interval where to find the eigenvalue.
  !-----------------------------------------------------------------------
  function wave_equation_emin(qn, wave_eq, potential)
    type(qn_t),         intent(in) :: qn
    integer,            intent(in) :: wave_eq
    type(potential_t),  intent(in) :: potential
    real(R8) :: wave_equation_emin

    real(R8) :: z

    call push_sub("wave_equation_emin")

    z = potential_nuclear_charge(potential)
    if (z == M_ZERO) then !We probably have a pseudopotential
      wave_equation_emin = potential_min(potential, qn)
    else !All-electron potential
      wave_equation_emin = 1.1_r8*hydrogen_eigenvalue(qn, wave_eq, z)
    end if

    call pop_sub()
  end function wave_equation_emin

  !-----------------------------------------------------------------------
  !> Returns the uppper bound of the interval where to find the eigenvalue
  !-----------------------------------------------------------------------
  function wave_equation_emax(qn, potential)
    type(qn_t),         intent(in) :: qn
    type(potential_t),  intent(in) :: potential
    real(R8) :: wave_equation_emax

    call push_sub("wave_equation_emax")

    wave_equation_emax = min(potential_max(potential, qn), -1e-6_r8)

    call pop_sub()
  end function wave_equation_emax

  !-----------------------------------------------------------------------
  !> We consider the wave-function to be zero if it is smaller than cutoff
  !-----------------------------------------------------------------------
  function wave_function_cutoff(integrator) result(cutoff)
    type(integrator_t), intent(in) :: integrator
    real(R8) :: cutoff

    cutoff = integrator%tol*1e-2_r8

  end function wave_function_cutoff

  !-----------------------------------------------------------------------
  !> Returns what is the set of ODEs that should be solved for a given    
  !> wave-equation.                                                       
  !-----------------------------------------------------------------------
  function wave_equation_to_ode(wave_eq, qn, potential)
    integer,           intent(in) :: wave_eq
    type(qn_t),        intent(in) :: qn
    type(potential_t), intent(in) :: potential
    integer :: wave_equation_to_ode

    call push_sub("wave_equation_to_ode")

    select case (wave_eq)
    case (SCHRODINGER)
      wave_equation_to_ode = ODE_SCHRODINGER
    case (SCALAR_REl)
      wave_equation_to_ode = ODE_SCALAR_REL
    case (DIRAC)
      if (qn%m == M_ZERO) then
        wave_equation_to_ode = ODE_DIRAC
      elseif  (abs(qn%m) == qn%l + M_HALF) then
        wave_equation_to_ode = ODE_DIRAC_POL1
      else
        if(potential_is_polarized(potential)) then
          wave_equation_to_ode = ODE_DIRAC_POL2
        else
          wave_equation_to_ode = ODE_DIRAC
        end if
      end if
    end select

    call pop_sub()
  end function wave_equation_to_ode

end module wave_equations_m
