!! Copyright (C) 2004-2011 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module troullier_martins_equations_m
  use global_m
  use messages_m
  use mesh_m
  use linalg_m
  use quantum_numbers_m
  use potentials_m
  implicit none


                    !---Global Variables---!

  !
  logical :: rel, silent
  integer :: wf_dim
  real(R8) :: rc, ev, rhs(7)
  type(mesh_t) :: m
  type(qn_t) :: qn

  !Stuff to compute the polynomial
  real(R8) :: p0_rc(6), p1_rc(6), p2_rc(6), p3_rc(6), p4_rc(6)
  real(R8), allocatable :: p0_r(:,:), p1_r(:,:), p2_r(:,:)

  !Work arrays
  real(R8), allocatable :: ps_wf(:,:), ps_wfp(:,:), ps_v(:)


                    !---Public/Private Statements---!

  private
  public :: tm_equations_init, &
            tm_equations, &
            tm_equations_end, &
            tm_solve_linear_system, &
            tm_ps_wavefunctions, &
            tm_equations_write_info


contains

  !-----------------------------------------------------------------------
  !> Initializes global data needed to solve the TM set of non-linear     
  !> equations.                                                           
  !-----------------------------------------------------------------------
  subroutine tm_equations_init(mp, rcp, relp, qnp, evp, rhsp, silentp)
    type(mesh_t), intent(in) :: mp      !< mesh
    real(R8),     intent(in) :: rcp     !< cutoff radius
    logical,      intent(in) :: relp    !< if true, use the relativistic extension to the scheme
    type(qn_t),   intent(in) :: qnp     !< set of quantum numbers
    real(R8),     intent(in) :: evp     !< 
    real(R8),     intent(in) :: rhsp(7) !< right-hand side of equations
    logical,      intent(in) :: silentp !< 

    integer  :: i
    real(R8), parameter :: FACTORS(6) = (/M_TWO, M_FOUR, M_SIX, M_EIGHT, M_TEN, M_TWELVE/)

    call push_sub("tm_equations_init")

    !
    call mesh_null(m)
    m = mp; rc = rcp; rel = relp; qn = qnp; ev = evp; rhs = rhsp; silent = silentp

    !
    allocate(p0_r(m%np, 6), p1_r(m%np, 6), p2_r(m%np, 6))  

    p0_r(:, 1) = m%r**2;  p1_r(:, 1) = m%r;     p2_r(:, 1) = M_ONE;
    p0_r(:, 2) = m%r**4;  p1_r(:, 2) = m%r**3;  p2_r(:, 2) = m%r**2;
    p0_r(:, 3) = m%r**6;  p1_r(:, 3) = m%r**5;  p2_r(:, 3) = m%r**4;
    p0_r(:, 4) = m%r**8;  p1_r(:, 4) = m%r**7;  p2_r(:, 4) = m%r**6;
    p0_r(:, 5) = m%r**10; p1_r(:, 5) = m%r**9;  p2_r(:, 5) = m%r**8;
    p0_r(:, 6) = m%r**12; p1_r(:, 6) = m%r**11; p2_r(:, 6) = m%r**10;

    do i = 1, 6
      p1_r(:, i) = p1_r(:, i)*FACTORS(i)
      p2_r(:, i) = p2_r(:, i)*(FACTORS(i) - M_ONE)*FACTORS(i)
    end do

    p0_rc(1) = rc**2;  p1_rc(1) = rc;     p2_rc(1) = M_ONE;  p3_rc(1) = M_ZERO; p4_rc(1) = M_ZERO
    p0_rc(2) = rc**4;  p1_rc(2) = rc**3;  p2_rc(2) = rc**2;  p3_rc(2) = rc;     p4_rc(2) = M_ONE
    p0_rc(3) = rc**6;  p1_rc(3) = rc**5;  p2_rc(3) = rc**4;  p3_rc(3) = rc**3;  p4_rc(3) = rc**2
    p0_rc(4) = rc**8;  p1_rc(4) = rc**7;  p2_rc(4) = rc**6;  p3_rc(4) = rc**5;  p4_rc(4) = rc**4
    p0_rc(5) = rc**10; p1_rc(5) = rc**9;  p2_rc(5) = rc**8;  p3_rc(5) = rc**7;  p4_rc(5) = rc**6
    p0_rc(6) = rc**12; p1_rc(6) = rc**11; p2_rc(6) = rc**10; p3_rc(6) = rc**9;  p4_rc(6) = rc**8

    p1_rc = p1_rc*FACTORS
    p2_rc = p2_rc*(FACTORS - M_ONE)*FACTORS
    p3_rc = p3_rc*(FACTORS - M_TWO)*(FACTORS - M_ONE)*FACTORS
    p4_rc = p4_rc*(FACTORS - M_THREE)*(FACTORS - M_TWO)*(FACTORS - M_ONE)*FACTORS

    wf_dim = qn_wf_dim(qn)
    allocate(ps_wf(m%np, wf_dim), ps_wfp(m%np, wf_dim), ps_v(m%np))

    call pop_sub()
  end subroutine tm_equations_init

  !-----------------------------------------------------------------------
  !> For a given set of coefficients x, returns the difference f_x between
  !> the left-hand side and the right-hand side of the equations.         
  !-----------------------------------------------------------------------
  subroutine tm_equations(n, x, f_x)
    integer,  intent(in)  :: n
    real(R8), intent(in)  :: x(n)
    real(R8), intent(out) :: f_x(n)

    real(R8) :: xx(7)

    xx(1) = x(1)
    xx(2:6) = M_ZERO
    xx(7) = x(2)

    !Solve linear system
    call tm_solve_linear_system(xx)

    !Compute the polynomial and the wavefunctions
    call tm_ps_wavefunctions(xx, ps_wf, ps_wfp, ps_v)

    !Return the values we want to be zero
    if (rel) then
      f_x(1) = mesh_integrate(m, sum(ps_wf**2, dim=2)) - rhs(7)
    else
      f_x(1) = mesh_integrate(m, ps_wf(:,1)**2) - rhs(7)
    end if
    f_x(2) = xx(2)**2 + xx(3)*(M_TWO*qn%l + M_FIVE) - rhs(6)

  end subroutine tm_equations

  !-----------------------------------------------------------------------
  !> Solve the linear part of the system of equations.                    
  !-----------------------------------------------------------------------
  subroutine tm_solve_linear_system(c)
    real(R8), intent(inout) :: c(7)

    real(R8) :: lsrhs(5), r(5,5)

    lsrhs(1) = rhs(1) - p0_rc(6)*c(7) - c(1)
    lsrhs(2) = rhs(2) - p1_rc(6)*c(7)
    lsrhs(3) = rhs(3) - p2_rc(6)*c(7)
    lsrhs(4) = rhs(4) - p3_rc(6)*c(7)
    lsrhs(5) = rhs(5) - p4_rc(6)*c(7)
    r(1,:) = p0_rc(1:5)
    r(2,:) = p1_rc(1:5)
    r(3,:) = p2_rc(1:5)
    r(4,:) = p3_rc(1:5)
    r(5,:) = p4_rc(1:5)
    call solve_linear_system(5, r, lsrhs, c(2:6))

  end subroutine tm_solve_linear_system

  !-----------------------------------------------------------------------
  !> For a given set of coefficients c, returns the corresponding pseudo  
  !> wave-functions and pseudo-potential.                                 
  !-----------------------------------------------------------------------
  subroutine tm_ps_wavefunctions(c, wf, wfp, pot)
    real(R8), intent(in) :: c(7)
    real(R8), intent(out) :: wf(m%np, wf_dim), wfp(m%np, wf_dim), pot(m%np)

    integer :: i
    real(R8) :: k
    real(R8), allocatable :: p(:), pp(:), ppp(:)
    type(potential_t) :: ps_potential

    !
    call potential_null(ps_potential)

    !Compute polynonial
    allocate(p(m%np), pp(m%np), ppp(m%np))
    p   = c(1)
    pp  = M_ZERO
    ppp = M_ZERO
    do i = 1, 6
      p   = p   + c(1+i)*p0_r(:,i)
      pp  = pp  + c(1+i)*p1_r(:,i)
      ppp = ppp + c(1+i)*p2_r(:,i)
    end do

    !Non-relativistic pseudopotential
    pot = ev + (real(qn%l,r8) + M_ONE)/m%r*pp + (ppp + pp**2)/M_TWO
    wf(:,1) = m%r**qn%l*exp(p)
    if (qn%l == 0) then
      wfp(:,1) = pp*exp(p)
    else
      wfp(:,1) = real(qn%l,R8)*m%r**(qn%l-1)*exp(p) + m%r**qn%l*pp*exp(p)
    end if

    if (rel) then
      !Relativistic correction to the pseudopotential
      k = -M_TWO*(qn%j - real(qn%l,R8))*(qn%j + M_HALF)
      call potential_init(ps_potential, m, pot)
      do i = 1, m%np
        pot(i) = pot(i) + (pot(i) - ev)**2/M_TWO/M_C2 + &
             dvdr(ps_potential, m%r(i), qn)/M_FOUR/M_C2*(wfp(i,1)/wf(i,1) + (k+M_ONE)/m%r(i))
      end do
      call potential_end(ps_potential)

      !Minor component
      wf(:,2) = M_C*((real(qn%l,R8) + M_ONE + k)/m%r + pp)*wf(:,1)/(M_TWO*M_C2 - pot + ev)
      wfp(:,2) = mesh_derivative(m, wf(:,2))
    else
      wf(:,2:wf_dim) = M_ZERO
      wfp(:,2:wf_dim) = M_ZERO
    end if
    deallocate(p, pp, ppp)

  end subroutine tm_ps_wavefunctions

  !-----------------------------------------------------------------------
  !> Writes to the screen some information about the multiroot solving.   
  !-----------------------------------------------------------------------
  subroutine tm_equations_write_info(iter, n, c, f)
    integer,  intent(in) :: iter, n
    real(R8), intent(in) :: c(n), f(n)

    real(R8) :: cc(7)

    ASSERT(n == 2)

    if (silent) return

    cc(1) = c(1)
    cc(2:6) = M_ZERO
    cc(7) = c(2)
    call tm_solve_linear_system(cc)

    write(message(1),'(6x,"Iteration number: ",I3)') iter
    write(message(2),'(6x,"Coefficients:")')
    write(message(3),'(8x,"c0  =",1x,es16.9e2,3x,"c2  =",1x,es16.9e2)') cc(1), cc(2)
    write(message(4),'(8x,"c4  =",1x,es16.9e2,3x,"c6  =",1x,es16.9e2)') cc(3), cc(4)
    write(message(5),'(8x,"c8  =",1x,es16.9e2,3x,"c10 =",1x,es16.9e2)') cc(5), cc(6)
    write(message(6),'(8x,"c12 =",1x,es16.9e2)') cc(7)
    write(message(7),'(6x,"Residues:")')
    write(message(8),'(8X,"c2**2 + c4(2l+5)    =",1x,ES16.9e2)') f(1)
    write(message(9),'(8X,"norm[AE] - norm[PP] =",1x,ES16.9e2)') f(2)
    message(10) = ""
    call write_info(10, 30)

  end subroutine tm_equations_write_info

  !-----------------------------------------------------------------------
  !> Frees all the memory associated to the global data needed to solve   
  !> the TM set of non-linear equations.                                  
  !-----------------------------------------------------------------------
  subroutine tm_equations_end()

    call push_sub("tm_equations_end")

    call mesh_end(m)
    deallocate(p0_r, p1_r, p2_r)
    deallocate(ps_wf, ps_wfp, ps_v)

    call pop_sub()
  end subroutine tm_equations_end

end module troullier_martins_equations_m
