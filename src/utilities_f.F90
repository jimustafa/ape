!! Copyright (C) 2004-2012 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

module utilities_m
  use global_m
  use iso_c_binding
  implicit none


                    !---Interfaces---!

  interface
    type(c_ptr) function cc_string() bind(c)
      import
    end function cc_string

    type(c_ptr) function fc_string() bind(c)
      import
    end function fc_string

    type(c_ptr) function cflags_string() bind(c)
      import
    end function cflags_string

    type(c_ptr) function fcflags_string() bind(c)
      import
    end function fcflags_string
    
    subroutine sys_getcwd(name)
      character(len=*), intent(out) :: name
    end subroutine sys_getcwd
    
    subroutine sys_mkdir(name)
      character(len=*), intent(in) :: name
    end subroutine sys_mkdir

    subroutine sys_rm(name)
      character(len=*), intent(in) :: name
    end subroutine sys_rm

    function sys_clock()
      real(8) :: sys_clock
    end function sys_clock

    subroutine sys_gettimeofday(sec, usec)
      integer, intent(out) :: sec, usec
    end subroutine sys_gettimeofday

    integer function sys_getmem()
    end function sys_getmem
  end interface


                    !---Public/Private Statements---!

  private
  public :: f_cc_string, &
            f_fc_string, &
            f_cflags_string, &
            f_fcflags_string, &
            locate, &
            str_center, &
            sys_getcwd, &
            sys_mkdir, &
            sys_rm, &
            sys_clock, &
            sys_gettimeofday, &
            sys_getmem

contains

  subroutine f_cc_string(cc)
    character(len=*), intent(out) :: cc

    type(c_ptr) :: c_cc
    
    c_cc = cc_string()
    call c_to_f_string_ptr(c_cc, cc)

  end subroutine f_cc_string

  subroutine f_fc_string(fc)
    character(len=*), intent(out) :: fc

    type(c_ptr) :: c_fc
    
    c_fc = fc_string()
    call c_to_f_string_ptr(c_fc, fc)

  end subroutine f_fc_string

  subroutine f_cflags_string(cflags)
    character(len=*), intent(out) :: cflags

    type(c_ptr) :: c_cflags
    
    c_cflags = cflags_string()
    call c_to_f_string_ptr(c_cflags, cflags)

  end subroutine f_cflags_string

  subroutine f_fcflags_string(fcflags)
    character(len=*), intent(out) :: fcflags

    type(c_ptr) :: c_fcflags
    
    c_fcflags = fcflags_string()
    call c_to_f_string_ptr(c_fcflags, fcflags)

  end subroutine f_fcflags_string

  
  !-----------------------------------------------------------------------
  !> Given an array xx and a value x, returns locate so x is between      
  !> xx(locate) and xx(locate+1). It may use a guess value for locate.    
  !> If guess <= 0 or > size xx then the guess is ignored.                
  !-----------------------------------------------------------------------
  function locate(xx, x, guess)
    real(R8), intent(in) :: x     !< value we want to locate
    real(R8), intent(in) :: xx(:) !< array of values where x is to be located. Must be monotonic
    integer,  intent(in) :: guess !< guess value for locate
    integer :: locate
    
    real(R8) :: s
    integer :: n, nr, nm, i, nl
     
    n = size(xx)
    s = sign(M_ONE, xx(n)-xx(1))

    !Check if x is out of bounds
    if(s*(xx(n) - x) < M_ZERO) then
      locate = n
      return
    elseif(s*(xx(1) - x) > M_ZERO) then
      locate = 0
      return
    end if
   
    if ( guess <= 0 .or. guess > n) then !Dont hunt. Just do bissection
      nl = 0
      nr = n + 1
    else !Start hunting
      i = 1
      if (s*(xx(guess)-x) <= M_ZERO) then
        nl=guess
        do
          if (nl == n) nl = n - 1
          nr = nl + i
          if(nr > n) nr = n
          if (s*(xx(nr) - x) >= M_ZERO) then
            exit
          else
            nl = nr
          end if
          i = 2*i
        end do
      elseif (s*(xx(guess) - x) > M_ZERO) then
        nr = guess
        do
          nl = nr - i
          if(nl < 1) nl = 1
          if (s*(xx(nl) - x) <= M_ZERO) then
            exit
          else
            nr = nl
          end if
          i = 2*i
        end do
      end if
    end if

    !Bissection
    do
      if(nr - nl == 1) then
        locate = nl
        exit
      end if
      nm = (nl + nr)/2
      if (s*(xx(nm) - x) < M_ZERO) then
        nl = nm
      elseif (s*(xx(nm) - x) > M_ZERO) then
        nr = nm
      else 
        locate = nm
        exit
      end if
    end do

  end function locate

  !-----------------------------------------------------------------------
  !>  Center a string.                                                    
  !-----------------------------------------------------------------------
  function str_center(s_in, l)
    character(len=*), intent(in) :: s_in !< string to center
    integer,          intent(in) :: l    !< length of the centered string
    character(len=80) :: str_center

    integer :: pad, i, li

    li = len(s_in)
    if(l < li) then
      str_center(1:l) = s_in(1:l)

    else  
      pad = (l - li)/2

      str_center = ""
      do i = 1, pad
        str_center(i:i) = " ";
      end do
      str_center(pad + 1:pad + li + 1) = s_in(1:li)
      do i = pad + li + 1, l
        str_center(i:i) = " ";
      end do
    end if

  end function str_center

  
  ! Helper functions to convert between C and Fortran strings
  ! Based on the routines by Joseph M. Krahn
  function f_to_c_string(f_string) result(c_string)
    character(len=*), intent(in) :: f_string
    character(kind=c_char,len=1) :: c_string(len_trim(f_string)+1)
      
    integer :: i, strlen

    strlen = len_trim(f_string)

    forall (i=1:strlen)
      c_string(i) = f_string(i:i)
    end forall
    c_string(strlen+1) = C_NULL_CHAR

  end function f_to_c_string

  subroutine c_to_f_string(c_string, f_string)
    character(kind=c_char,len=1), intent(in)  :: c_string(*)
    character(len=*),             intent(out) :: f_string

    integer :: i

    i = 1
    do while(c_string(i) /= C_NULL_CHAR .and. i <= len(f_string))
      f_string(i:i) = c_string(i)
      i = i + 1
    end do
    if (i < len(f_string)) f_string(i:) = ' '

  end subroutine c_to_f_string

  subroutine c_to_f_string_ptr(c_string, f_string)
    type(c_ptr),      intent(in)  :: c_string
    character(len=*), intent(out) :: f_string

    character(len=1, kind=c_char), pointer :: p_chars(:)
    integer :: i

    if (.not. c_associated(c_string)) then
      f_string = ' '
    else
      call c_f_pointer(c_string, p_chars, [huge(0)])
      i = 1
      do while(p_chars(i) /= C_NULL_CHAR .and. i <= len(f_string))
        f_string(i:i) = p_chars(i)
        i = i + 1
      end do
      if (i < len(f_string)) f_string(i:) = ' '
    end if

  end subroutine c_to_f_string_ptr
  
end module utilities_m
