!! Copyright (C) 2004-2014 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module sl_potentials_m
  use global_m
  use messages_m
  use io_m
  use units_m
  use splines_m
  use mesh_m
  use quantum_numbers_m
  use ps_io_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
     module procedure sl_potential_copy
  end interface


                    !---Derived Data Types---!

  type sl_potential_t
    private
    integer :: nc
    type(qn_t),     pointer :: qn(:)
    real(R8),       pointer :: v(:,:)
    type(spline_t), pointer :: v_spl(:)
    type(spline_t), pointer :: vp_spl(:)
    type(spline_t), pointer :: vpp_spl(:)
  end type sl_potential_t


                    !---Public/Private Statements---!

  private
  public :: sl_potential_t, &
            sl_potential_null, &
            sl_potential_init, &
            assignment(=), &
            sl_potential_end, &
            sl_potential_save, &
            sl_potential_load, &
            sl_v, &
            sl_dvdr, &
            sl_d2vdr2, &
            sl_potential_debug, &
            sl_potential_output, &
            sl_potential_ps_io_set


contains

  !-----------------------------------------------------------------------
  !> Nullifies and sets to zero all the components of the potential.      
  !-----------------------------------------------------------------------
  subroutine sl_potential_null(potential)
    type(sl_potential_t), intent(out) :: potential

    call push_sub("sl_potential_null")

    potential%nc = 0
    nullify(potential%qn)
    nullify(potential%v)
    nullify(potential%v_spl)
    nullify(potential%vp_spl)
    nullify(potential%vpp_spl)

    call pop_sub()
  end subroutine sl_potential_null

  !-----------------------------------------------------------------------
  !> Initialize a semi-local potential                                    
  !-----------------------------------------------------------------------
  subroutine sl_potential_init(potential, m, nc, qn, v)
    type(sl_potential_t), intent(inout) :: potential   !< potential to be initialized
    type(mesh_t),         intent(in)    :: m           !< mesh
    integer,              intent(in)    :: nc          !< number of components
    type(qn_t),           intent(in)    :: qn(nc)      !< quantum numbers of each component
    real(R8),             intent(in)    :: v(m%np, nc) !< values of the potentials on the mesh for each component

    integer :: i, k

    call push_sub("sl_potential_init")

    potential%nc = nc
    allocate(potential%qn(nc))
    allocate(potential%v(m%np, nc))
    allocate(potential%v_spl(nc))
    allocate(potential%vp_spl(nc))
    allocate(potential%vpp_spl(nc))
    do i = 1, nc
      k = qn(i)%l + int(qn(i)%j) + 1
      potential%qn(k) = qn(i)
      potential%v(:, k) = v(:, i)
      call spline_null(potential%v_spl(k))
      call spline_null(potential%vp_spl(k))
      call spline_null(potential%vpp_spl(k))
      call spline_init(potential%v_spl(k), m%np, m%r, potential%v(:, k), 3)
      call spline_init(potential%vp_spl(k), m%np, m%r, mesh_derivative(m, potential%v(:, k)), 3)
      call spline_init(potential%vpp_spl(k), m%np, m%r, mesh_derivative2(m, potential%v(:, k)), 3)
    end do

    call pop_sub()
  end subroutine sl_potential_init

  !-----------------------------------------------------------------------
  !> Copies the potential potential_a to potential potential_b.           
  !-----------------------------------------------------------------------
  subroutine sl_potential_copy(potential_a, potential_b)
    type(sl_potential_t), intent(inout) :: potential_a
    type(sl_potential_t), intent(in)    :: potential_b

    integer :: i

    call push_sub("sl_potential_copy")

    call sl_potential_end(potential_a)

    potential_a%nc = potential_b%nc

    allocate(potential_a%qn(potential_a%nc))
    potential_a%qn = potential_b%qn

    allocate(potential_a%v(size(potential_b%v, dim=1), potential_a%nc))
    potential_a%v = potential_b%v
    allocate(potential_a%v_spl(potential_a%nc))
    allocate(potential_a%vp_spl(potential_a%nc))
    allocate(potential_a%vpp_spl(potential_a%nc))
    do i = 1, potential_a%nc
      call spline_null(potential_a%v_spl(i))
      call spline_null(potential_a%vp_spl(i))
      call spline_null(potential_a%vpp_spl(i))
      potential_a%v_spl(i) = potential_b%v_spl(i)
      potential_a%vp_spl(i) = potential_b%vp_spl(i)
      potential_a%vpp_spl(i) = potential_b%vpp_spl(i)
    end do

    call pop_sub()
  end subroutine sl_potential_copy

  subroutine sl_potential_end(potential)
    !-----------------------------------------------------------------------!
    ! Frees all the memory associated to the potential.                     !
    !-----------------------------------------------------------------------!
    type(sl_potential_t), intent(inout) :: potential

    integer :: i

    call push_sub("sl_potential_end")

    if (associated(potential%qn)) then
      deallocate (potential%qn)
    end if
    if (associated(potential%v)) then
      deallocate (potential%v)
    end if
    if (associated(potential%v_spl)) then
      do i = 1, potential%nc
        call spline_end(potential%v_spl(i))
      end do
      deallocate(potential%v_spl)
    end if
    if (associated(potential%vp_spl)) then
      do i = 1, potential%nc
        call spline_end(potential%vp_spl(i))
      end do
      deallocate(potential%vp_spl)
    end if
    if (associated(potential%vpp_spl)) then
      do i = 1, potential%nc
        call spline_end(potential%vpp_spl(i))
      end do
      deallocate(potential%vpp_spl)
    end if

    call pop_sub()
  end subroutine sl_potential_end

  !-----------------------------------------------------------------------
  !> Writes the potential to a file.                                      
  !-----------------------------------------------------------------------
  subroutine sl_potential_save(unit, m, potential)
    integer,              intent(in) :: unit      !< file unit number
    type(mesh_t),         intent(in) :: m         !< mesh
    type(sl_potential_t), intent(in) :: potential !< potential to be written

    integer :: k, i

    call push_sub("sl_potential_save")

    write(unit) potential%nc
    do k = 1, potential%nc
      write(unit) potential%qn(k)
      do i = 1, m%np
        write(unit) potential%v(i, k)
      end do
    end do

    call pop_sub()
  end subroutine sl_potential_save

  !-----------------------------------------------------------------------
  !> Reads the potential from a file.                                     
  !-----------------------------------------------------------------------
  subroutine sl_potential_load(unit, m, potential)
    integer,              intent(in)    :: unit      !< file unit number
    type(mesh_t),         intent(in)    :: m         !< mesh
    type(sl_potential_t), intent(inout) :: potential !< potential to be read

    integer :: k, i

    call push_sub("sl_potential_load")

    read(unit) potential%nc
    allocate(potential%qn(potential%nc))
    allocate(potential%v(m%np, potential%nc))
    allocate(potential%v_spl(potential%nc))
    allocate(potential%vp_spl(potential%nc))
    allocate(potential%vpp_spl(potential%nc))
    do k = 1, potential%nc
      read(unit) potential%qn(k)
      do i = 1, m%np
        read(unit) potential%v(i, k)
      end do
      call spline_null(potential%v_spl(k))
      call spline_null(potential%vp_spl(k))
      call spline_null(potential%vpp_spl(k))
      call spline_init(potential%v_spl(k), m%np, m%r, potential%v(:, k), 3)
      call spline_init(potential%vp_spl(k), m%np, m%r, mesh_derivative(m, potential%v(:, k)), 3)
      call spline_init(potential%vpp_spl(k), m%np, m%r, mesh_derivative2(m, potential%v(:, k)), 3)
    end do

    call pop_sub()
  end subroutine sl_potential_load

  !-----------------------------------------------------------------------
  !> Returns the value of the potential felt by an electron at radius r.  
  !> If the potential is j-averaged and qn%j = 0, then the j-averaged     
  !> pseudopotential is used. If j = l +- 1/2 we assume that the          
  !> spin-orbit part should also be aplied. In that case, the j-dependent 
  !> pseudopotentials are used, which in this case is perfectly equivalent
  !-----------------------------------------------------------------------
  function sl_v(potential, r, qn)
    type(sl_potential_t), intent(in) :: potential
    real(R8),             intent(in) :: r
    type(qn_t),           intent(in) :: qn
    real(R8) :: sl_v

    sl_v = spline_eval(potential%v_spl(qn%l + int(qn%j) + 1), r)

  end function sl_v

  !-----------------------------------------------------------------------
  !> Returns the value of the first derivative of the potential felt by an
  !> electron at radius r.                                                
  !-----------------------------------------------------------------------
  function sl_dvdr(potential, r, qn)
    type(sl_potential_t), intent(in) :: potential
    real(R8),             intent(in) :: r
    type(qn_t),           intent(in) :: qn
    real(R8) :: sl_dvdr

    sl_dvdr = spline_eval(potential%vp_spl(qn%l + int(qn%j) + 1), r)

  end function sl_dvdr

  !-----------------------------------------------------------------------
  !> Returns the value of the second derivative of the potential felt by  
  !> an electron  radius r.                                               
  !-----------------------------------------------------------------------
  function sl_d2vdr2(potential, r, qn)
    type(sl_potential_t), intent(in) :: potential
    real(R8),             intent(in) :: r
    type(qn_t),           intent(in) :: qn
    real(R8) :: sl_d2vdr2

    sl_d2vdr2 = spline_eval(potential%vpp_spl(qn%l + int(qn%j) + 1), r)

  end function sl_d2vdr2

  !-----------------------------------------------------------------------
  !> Prints debug information to the "debug_info" directory.              
  !-----------------------------------------------------------------------
  subroutine sl_potential_debug(potential)
    type(sl_potential_t), intent(in) :: potential

    call push_sub("sl_potential_debug")

    call pop_sub()
  end subroutine sl_potential_debug

  !-----------------------------------------------------------------------
  !> Writes the potential to a file in a format suitable for plotting.    
  !-----------------------------------------------------------------------
  subroutine sl_potential_output(potential, m, dir)
    type(sl_potential_t), intent(in) :: potential
    type(mesh_t),         intent(in) :: m
    character(len=*),     intent(in) :: dir

    integer  :: i, k, unit
    real(R8) :: ue, ul
    character(len=10) :: label
    character(len=20) :: filename

    call push_sub("sl_potential_output")

    ul = units_out%length%factor
    ue = units_out%energy%factor

    do k = 1, potential%nc
      label = qn_label(potential%qn(k))
      filename = trim(dir)//"/pp-"//trim(label(2:))
      call io_open(unit, file=trim(filename))

      write(unit,'("# ")')
      write(unit,'("# Energy units: ",A)') trim(units_out%energy%name)
      write(unit,'("# Length units: ",A)') trim(units_out%length%name)
      write(unit,'("#")')
      write(unit,'("# ",34("-"))')
      write(unit,'("# |",7X,"r",7X,"|",6X,"v(r)",6X,"|")')
      write(unit,'("# ",34("-"))')
      do i = 1, m%np
        write(unit,'(3X,ES14.8E2,3X,ES15.8E2)') m%r(i)/ul, &
             sl_v(potential, m%r(i), potential%qn(k))/ue
      end do
      close(unit)
    end do

    call pop_sub()
  end subroutine sl_potential_output

  !-----------------------------------------------------------------------
  !> Pass the information about the pseudopotentials to the ps_io module. 
  !-----------------------------------------------------------------------
  subroutine sl_potential_ps_io_set(potential, m)
    type(sl_potential_t), intent(in) :: potential
    type(mesh_t),         intent(in) :: m

    call ps_io_set_psp(m%np, potential%nc, potential%qn%l, potential%qn%j, potential%v)

  end subroutine sl_potential_ps_io_set

end module sl_potentials_m
