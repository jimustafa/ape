!! Copyright (C) 2004-2011 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module hamann_m
  use global_m
  use messages_m
  use io_m
  use output_m
  use mesh_m
  use quantum_numbers_m
  use potentials_m
  use wave_equations_m
  implicit none


                    !---Derived Data Types---!

  type ld_cl
    real(R8) :: cl
    real(R8) :: ldd
    integer  :: nnodes
    type(ld_cl), pointer :: next
    type(ld_cl), pointer :: prev
  end type ld_cl


                    !---Global Variables---!

  real(R8), parameter   :: LAMBDA = 3.5_r8
  real(R8), parameter   :: M_TOL = 1.0e-18_r8


                    !---Public/Private Statements---!

  private
  public :: hamann_gen, &
            hamann_match_radius


contains

  !-----------------------------------------------------------------------
  !> Generates the pseudo wave-functions and the corresponding            
  !> pseudo-potential using the Hamann scheme.                            
  !-----------------------------------------------------------------------
  subroutine hamann_gen(qn, ev, wave_eq, tol, m, ae_potential, rc, integrator_sp,&
                        integrator_dp, ps_v, ps_wf, ps_wfp)
    type(qn_t),         intent(inout) :: qn            !< quantum numbers of valence state (all electron on input and pseudo on output)
    real(R8),           intent(in)    :: ev            !< eigenvalue of valence state
    integer,            intent(in)    :: wave_eq       !< wave equation to use
    real(R8),           intent(in)    :: tol           !< tolerance
    type(mesh_t),       intent(in)    :: m             !< mesh
    type(potential_t),  intent(in)    :: ae_potential  !< all-electron potential
    real(R8),           intent(in)    :: rc            !< cutoff radius
    type(integrator_t), intent(inout) :: integrator_sp !< single-precision integrator object
    type(integrator_t), intent(inout) :: integrator_dp !< double-precision integrator object
    real(R8),           intent(out)   :: ps_v(m%np)    !< pseudo-potential on the mesh
    real(R8),           intent(inout) :: ps_wf(:, :)   !< wavefunction (all electron on input and pseudo on output)
    real(R8),           intent(inout) :: ps_wfp(:, :)  !< wavefunction derivative (all electron on input and pseudo on output)

    integer               :: wf_dim, i, ierr
    real(R8)              :: ae_ld, aa, bb, cc, gamma, r, dlt, cl, factor
    real(R8), allocatable :: f(:), vc(:), g(:), w2(:), v2(:)
    real(R8), allocatable :: u(:,:), up(:,:), w1(:,:), w1p(:,:)
    type(mesh_t)      :: mr
    type(ld_cl)       :: cl_bracket(2)
    type(potential_t) :: v1, ps_potential

    call push_sub("hamann_gen")

    !
    call mesh_null(mr)
    call potential_null(v1)
    call potential_null(ps_potential)

    wf_dim = qn_wf_dim(qn)

    !New mesh
    mr = m
    call mesh_truncate(mr, hamann_match_radius(m, rc))
    r = mr%r(mr%np)
    allocate(f(mr%np), vc(mr%np), g(mr%np), w2(mr%np), v2(m%np))
    allocate(u(mr%np, wf_dim), up(mr%np, wf_dim), w1(mr%np, wf_dim), w1p(mr%np, wf_dim))

    !Cutoff function
    do i = 1, mr%np
      f(i) = exp(-(mr%r(i)/rc)**LAMBDA)
      vc(i) = (1 - f(i)) * v(ae_potential, mr%r(i), qn)
    end do

    !Get logaritmic derivative and the all-electron wavefunction value at rc
    call hamann_wavefunction(qn, ev, wave_eq, mr, ae_potential, integrator_dp, u, up)
    ae_ld = up(mr%np,1)/u(mr%np,1)

    !Bracket cl
    ierr = bracket_cl(qn, ev, mr, f, vc, ae_ld, v(ae_potential, rc, qn), tol, integrator_sp, cl_bracket)
    if (ierr /= 0) then
      message(1) = "Unable to bracket cl in subroutine bracket_cl!"
      call write_fatal(1)
    end if

    !Find cl more accuratly
    cl = find_cl(qn, ev, mr, f, vc, ae_ld, tol, integrator_dp, cl_bracket)

    !Get intermediate pseudo-potential and pseudo-wavefunction
    call potential_init(v1, mr, vc + cl*f)
    call hamann_wavefunction(qn, ev, SCHRODINGER, mr, v1, integrator_dp, w1, w1p)

    !Get final pseudo-wavefunction
    gamma = u(mr%np,1)/w1(mr%np,1)
    g = f*mr%r**qn%l
    aa = gamma**2*mesh_integrate(mr, g**2, b=r)
    bb = M_TWO*gamma**2*mesh_integrate(mr, w1(:,1)*g, b=r)
    cc = gamma**2 - M_ONE
    dlt = (-bb + bb/abs(bb)*sqrt(bb**2 - M_FOUR*aa*cc))/M_TWO/aa
    w2 = gamma * (w1(:,1) + dlt*g)

    !Invert Schrodinger equation
    do i = 1, mr%np
      v2(i) = v(v1, m%r(i), qn) + gamma*dlt*g(i)/M_TWO/w2(i)* &
                       (LAMBDA**2/m%r(i)**2*(m%r(i)/rc)**(M_TWO*LAMBDA) - &
                       (M_TWO*LAMBDA*qn%l + LAMBDA*(LAMBDA + M_ONE))/m%r(i)**2* &
                       (m%r(i)/rc)**LAMBDA + M_TWO*ev - M_TWO*v(v1, m%r(i), qn))
    end do
    do i = mr%np+1, m%np
      v2(i) = v(ae_potential, m%r(i), qn)
    end do

    !Get final pseudo-potential and pseudo-wavefunction
    qn%n = qn%l + 1
    ps_v = v2
    call potential_init(ps_potential, m, ps_v)
    call wavefunctions(qn, ev, wave_eq, mr, ps_potential, integrator_dp, u, up)
    do i = 1, wf_dim
      factor = u(mr%np,i)/ps_wf(mr%np,i)
      ps_wf(1:mr%np,i) = u(1:mr%np,i)/factor
      factor = up(mr%np,i)/ps_wfp(mr%np,i)
      ps_wfp(1:mr%np,i) = up(1:mr%np,i)/factor
    end do
    factor = wavefunctions_norm(wave_eq, m, ps_wf)
    ps_wf = ps_wf/sqrt(factor)
    ps_wfp = ps_wfp/sqrt(factor)

    !Write info
    write(message(1),'(4x,"Core radius:",1x,f7.3,5x,"Matching Radius:",1x,f7.3)') rc, mr%r(mr%np)
    write(message(2),'(4x,"cl  =",1x,f16.10)') cl
    call write_info(2,20)
    call write_info(2,unit=info_unit("pp"))

    !End potentials and deallocate arrays
    call mesh_end(mr)
    call potential_end(v1)
    call potential_end(ps_potential)
    deallocate(f, vc, u, up, w1, w1p, g, w2, v2)

    call pop_sub()
  end subroutine hamann_gen

  !-----------------------------------------------------------------------
  !> Bracket the parameter cl of the Hamann potential:                    
  !>  V_1 = (1-f)*V_ae + cl*f                                             
  !-----------------------------------------------------------------------
  function bracket_cl(qn, ev, mr, f, vc, ae_ld, cl_guess, tol, integrator, cl_bracket)
    type(qn_t),         intent(in)    :: qn            !< set of quantum numbers
    real(R8),           intent(in)    :: ev            !< eigenvalue
    type(mesh_t),       intent(in)    :: mr            !< truncated mesh
    real(R8),           intent(in)    :: f(mr%np)      !< f(r) = exp(-(r/rcl)**lambda))
    real(R8),           intent(in)    :: vc(mr%np)     !< (1-f(r))*V_ae
    real(R8),           intent(in)    :: ae_ld         !< all-electron wave-function logarithmic derivative at rc
    real(R8),           intent(in)    :: cl_guess      !< initial guess for cl
    real(R8),           intent(in)    :: tol           !< tolerance
    type(integrator_t), intent(inout) :: integrator    !< integrator object
    type(ld_cl),        intent(out)   :: cl_bracket(2) !< information about upper and lower bounds of the interval containing cl
    integer :: bracket_cl !< error code

    real(R8)          :: h
    type(potential_t) :: v1
    type(ld_cl), pointer :: ptr, next, prev, new_ptr

    call push_sub("bracket_cl")

    !
    nullify(ptr, next, prev, new_ptr)
    call potential_null(v1)

    !Initial values
    allocate(ptr)
    call ld_cl_null(ptr)
    allocate(next)
    call ld_cl_null(next)
    ptr%cl = cl_guess - M_ONE
    next%cl = cl_guess + M_ONE
    ptr%next => next
    next%prev => ptr
    do
      call potential_init(v1, mr, vc + ptr%cl*f)
      ptr%ldd = hamann_ld(qn, ev, SCHRODINGER, mr, v1, integrator, ptr%nnodes) - ae_ld
      if (.not. associated(ptr%next)) exit
      call potential_end(v1)
      ptr => ptr%next
    end do

    !Expand brackets to the left until we have wavefunctions with one node
    call goto_first(ptr)
    next => ptr%next
    do
      if (ptr%nnodes >= 1) exit

      allocate(new_ptr)
      call ld_cl_null(new_ptr)
      ptr%prev => new_ptr
      new_ptr%next => ptr
      new_ptr%cl = ptr%cl - M_TWO*abs(next%cl - ptr%cl)
      call potential_end(v1)
      call potential_init(v1, mr, vc + new_ptr%cl*f)
      new_ptr%ldd = hamann_ld(qn, ev, SCHRODINGER, mr, v1, integrator, new_ptr%nnodes) - ae_ld
      
      ptr => new_ptr
      next => ptr%next
      nullify(new_ptr)
    end do

    !Expand brackets to the right until we have wavefunctions with no nodes and a positive value for the logaritmic derivative diference
    call goto_last(ptr)
    prev => ptr%prev
    do
      if (ptr%cl > M_ZERO .and. ptr%nnodes == 0) exit

      allocate(new_ptr)
      call ld_cl_null(new_ptr)
      ptr%next => new_ptr
      new_ptr%prev => ptr
      new_ptr%cl = ptr%cl + M_TWO*abs(ptr%cl - prev%cl)
      call potential_end(v1)
      call potential_init(v1, mr, vc + new_ptr%cl*f)
      new_ptr%ldd = hamann_ld(qn, ev, SCHRODINGER, mr, v1, integrator, new_ptr%nnodes) - ae_ld

      ptr => new_ptr
      prev => ptr%prev
      nullify(new_ptr)
    end do

    call goto_last(ptr)
    h = ptr%cl
    call goto_first(ptr)
    h = abs(h - ptr%cl)
    next => ptr%next
    main: do

      !Check if the cl has been bracketed 
      do
        if (ptr%ldd*next%ldd < M_ZERO .and. ptr%nnodes == 0 .and. next%nnodes == 0) then
          cl_bracket(1) = ptr
          cl_bracket(2) = next
          exit main
        end if
        ptr => next
        if (.not. associated(ptr%next)) exit
        next => ptr%next
      end do

      if (abs(h) < sqrt(tol)) then !Theres something wrong
        bracket_cl = 1
        if (in_debug_mode) then
          call bracket_cl_debug()
          call potential_debug(v1)
        end if
        call potential_end(v1)
        call end_ptr(ptr)
        call pop_sub()
        return
      end if

      !Refine brackets
      h = h*M_HALF
      call goto_first(ptr)
      next => ptr%next
      do
        allocate(new_ptr)
        call ld_cl_null(new_ptr)
        new_ptr%cl = ptr%cl + (next%cl - ptr%cl)*M_HALF
        call potential_end(v1)
        call potential_init(v1, mr, vc + new_ptr%cl*f)
        new_ptr%ldd = hamann_ld(qn, ev, SCHRODINGER, mr, v1, integrator, new_ptr%nnodes) - ae_ld
        new_ptr%prev => ptr
        new_ptr%next => next
        ptr%next => new_ptr
        next%prev => new_ptr
        nullify(new_ptr)
        ptr => next   
        if (.not. associated(ptr%next)) exit
        next => ptr%next
      end do

      !Remove brackets with wrong number of nodes
      call goto_first(ptr)
      next => ptr%next
      do
        if (next%nnodes >= 1) then
          nullify(next%prev)
          deallocate(ptr)
        end if
        ptr => next
        if (.not. associated(ptr%next)) exit
        next => ptr%next
        if (next%nnodes == 0) exit
      end do

    end do main

    !Deallocate everything
    call end_ptr(ptr)
    call potential_end(v1)

    bracket_cl = 0

    call pop_sub()

  contains

    subroutine bracket_cl_debug()
      integer :: unit

      call io_open(unit, file='debug_info/bracket_cl')

      call goto_first(ptr)
      do
        write(unit,'(F12.5,1X,ES10.3E2,1X,I2)') ptr%cl, ptr%ldd, ptr%nnodes
        if (.not. associated(ptr%next)) exit
        ptr => ptr%next
      end do
      close(unit)

    end subroutine bracket_cl_debug

  end function bracket_cl

  subroutine ld_cl_null(ptr)
    type(ld_cl), pointer :: ptr

    ptr%cl = M_ZERO
    ptr%ldd = M_ZERO
    ptr%nnodes = 0
    nullify(ptr%next)
    nullify(ptr%prev)

  end subroutine ld_cl_null

  subroutine goto_first(ptr)
    type(ld_cl), pointer :: ptr

    do
      if (.not. associated(ptr%prev)) exit
      ptr => ptr%prev
    end do

  end subroutine goto_first

  subroutine goto_last(ptr)
    type(ld_cl), pointer :: ptr
        
    do
      if (.not. associated(ptr%next)) exit
      ptr => ptr%next
    end do

  end subroutine goto_last

  subroutine end_ptr(ptr)
    type(ld_cl), pointer :: ptr
    
    type(ld_cl), pointer :: next

    call goto_first(ptr)
    do
      if (.not. associated(ptr%next)) exit
      next => ptr%next
      nullify(next%prev)
      deallocate(ptr)
      ptr => next
    end do
    deallocate(ptr)

  end subroutine end_ptr

  !-----------------------------------------------------------------------
  !> Use Brents method to find the parameter cl.                          
  !-----------------------------------------------------------------------
  function find_cl(qn, ev, mr, f, vc, ae_ld, tol, integrator, cl_bracket)
    type(qn_t),         intent(in)    :: qn            !< set of quantum numbers
    real(R8),           intent(in)    :: ev            !< eigenvalue
    type(mesh_t),       intent(in)    :: mr            !< truncated mesh
    real(R8),           intent(in)    :: f(mr%np)      !< f(r) = exp(-(r/rcl)**lambda))
    real(R8),           intent(in)    :: vc(mr%np)     !< (1-f(r))*V_ae
    real(R8),           intent(in)    :: ae_ld         !< all-electron wave-function logarithmic derivative at rc
    real(R8),           intent(in)    :: tol           !< tolerance
    type(integrator_t), intent(inout) :: integrator    !< integrator object
    type(ld_cl),        intent(in)    :: cl_bracket(2) !< information about upper and lower bounds of the interval containing cl
    real(R8) :: find_cl !< error code

    integer             :: nnodes_dum
    real(R8)            :: a, b, c, fa, fb, fc, tol1, m, d, e, r, s, q, p
    real(R8), parameter :: eps = epsilon(M_ONE)
    type(potential_t)   :: v1

    call push_sub("find_cl")

    call potential_null(v1)

    a = cl_bracket(1)%cl
    b = cl_bracket(2)%cl
    fa = cl_bracket(1)%ldd
    fb = cl_bracket(2)%ldd
    c = b
    fc = fb
    call potential_init(v1, mr, vc)

    do
      if ((fb < M_ZERO .and. fc < M_ZERO) .or. (fb > M_ZERO .and. fc > M_ZERO)) then
        c = a; fc = fa
        d = b - a; e = d
      end if

      if (abs(fc) < abs(fb)) then
        a = b; b = c; c = a
        fa = fb; fb = fc; fc = fa
      end if
      tol1 = M_TWO*eps*abs(b) + M_HALF*tol
      m = (c - b)*M_HALF

      if (abs(m) <= tol .or. fb == M_ZERO) exit

      if (abs(e) > tol .and. abs(fa) > abs(fb)) then
        s = fb/fa
        if (a == c) then !Linear interpolation
          p = M_TWO*m*s
          q = M_ONE - s
        else !Inverse quadratic interpolation
          q = fa / fc
          r = fb / fc
          p = s*(M_TWO*m*q*(q - r) - (b - a)*(r - M_ONE))
          q = (q - M_ONE)*(r - M_ONE)*(s - M_ONE)           
        end if
        if (p > M_ZERO) then
          q = - q
        else
          p = abs(p)
        end if
        s = e
        e = d
        if (M_TWO*p < min(M_THREE*m*q - abs(tol1 * q), abs(M_HALF*s*q))) then
          d = p/q
        else
          d = m
          e = d
        end if
      end if

      a = b
      fa = fb
      if (abs(d) > tol1) then
        b = b + d
      else
        b = b + sign(tol1, m)
      end if

      call potential_end(v1)
      call potential_init(v1, mr, vc+b*f)
      fb = hamann_ld(qn, ev, SCHRODINGER, mr, v1, integrator, nnodes_dum) - ae_ld
    end do

    call potential_end(v1)
    find_cl = b 

    call pop_sub()
  end function find_cl

  !-----------------------------------------------------------------------
  !> Returns the radius from which the all-electron and the pseudo        
  !> wave-functions should be equal.                                      
  !-----------------------------------------------------------------------
  function hamann_match_radius(m, rc)
    type(mesh_t), intent(in) :: m
    real(R8),     intent(in) :: rc
    real(R8) :: hamann_match_radius

    hamann_match_radius = minval(m%r, exp(-(m%r/rc)**LAMBDA) < M_TOL)

  end function hamann_match_radius

end module hamann_m
