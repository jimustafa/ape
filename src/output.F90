!! Copyright (C) 2004-2011 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module output_m
  use global_m
  use io_m
  use messages_m
  implicit none


                    !---Derived Data Types---!

  type info_file_t
    integer :: unit
    character(10) :: dir
  end type info_file_t


                    !---Global Variables---!

  integer :: n_files = 0
  type(info_file_t), pointer :: info_files(:)


                    !---Public/Private Statements---!

  private
  public :: output_init, &
            info_unit, &
            output_end

contains

  !-----------------------------------------------------------------------
  !> Creates the output directory and opens the file "info".              
  !-----------------------------------------------------------------------
  subroutine output_init(dir)
    character(len=*), intent(in) :: dir

    type(info_file_t), allocatable :: tmp(:)

    call push_sub("output_init")

    !Create directory
    call sys_mkdir(trim(dir))

    !Open file
    if (n_files == 0) then
      n_files = 1
      allocate(info_files(1))
    else
      allocate(tmp(n_files))
      tmp = info_files
      deallocate(info_files)
      allocate(info_files(n_files+1))
      info_files(1:n_files) = tmp(1:n_files)
      n_files = n_files + 1
    end if
    info_files(n_files)%dir = trim(dir)
    call io_open(info_files(n_files)%unit, trim(dir)//"/info")

    call pop_sub()
  end subroutine output_init

  !-----------------------------------------------------------------------
  !> Returns the unit number for the "dir/info" file.                     
  !-----------------------------------------------------------------------
  function info_unit(dir)
    character(len=*), intent(in) :: dir
    integer :: info_unit

    integer :: i

    ASSERT(n_files > 0)

    do i = 1, n_files
      if (trim(info_files(i)%dir) == trim(dir)) then
        info_unit = info_files(i)%unit
        exit
      end if
    end do

  end function info_unit

  !-----------------------------------------------------------------------
  !> Closes the "dir/info" file.                                          
  !-----------------------------------------------------------------------
  subroutine output_end(dir)
    character(len=*), intent(in) :: dir

    integer :: i
    type(info_file_t), allocatable :: tmp(:)

    call push_sub("output_end")

    ASSERT(n_files > 0)

    if (n_files == 1) then
      close(info_files(1)%unit)
      deallocate(info_files)
      n_files = 0
    else
      do i = 1, n_files
        if (trim(info_files(i)%dir) == trim(dir)) exit
      end do
      close(info_files(i)%unit)
      allocate(tmp(n_files-1))
      tmp(1:i-1) = info_files(1:i-1)
      tmp(i:n_files-1) = info_files(i+1:n_files)
      deallocate(info_files)
      allocate(info_files(n_files-1))
      info_files = tmp
      n_files = n_files - 1
    end if

    call pop_sub()
  end subroutine output_end

end module output_m
