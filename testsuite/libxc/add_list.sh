#!/bin/bash

LIST=$1
APE=$2

for i in $(seq $(wc -l $LIST | awk '{print $1}')); do
    LINE=$(awk "NR == $i" $LIST | sed 's/\#.*//g')
    FUNC_ID=$(echo $LINE | awk '{print $1}')
    FUNC_FAMILY=$(echo $LINE | awk '{print $2}')
    FUNC_TYPE=$(echo $LINE | awk '{print $3}')
    FUNC_NAME=$(echo $LINE | awk '{print $4}')
    RUN=$(echo $LINE | awk '{print $5}')
    if [ "$FUNC_NAME" == "Y" ]; then
	RUN=$FUNC_NAME
	FUNC_NAME=' '
    fi
    if [ "$RUN" == "Y" ]; then
	echo $FUNC_NAME
	./add_func.pl $APE $FUNC_ID $FUNC_FAMILY $FUNC_TYPE "$FUNC_NAME"
    fi
done
