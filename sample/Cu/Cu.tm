# Generalities
Title = "Copper"
CalculationMode = pp
Verbose = 40

# PseudoPotentials
%PPComponents
  4  |  0  | 2.08 | tm
  4  |  1  | 2.30 | tm
  3  |  2  | 2.08 | tm
%

PPCalculationTolerance = 1e-6
LLocal = 0

PPOutputFileFormat = abinit6 + fhi

# Wave-equations solver
EigenSolverTolerance = 1e-9
ODEIntTolerance = 1e-14
